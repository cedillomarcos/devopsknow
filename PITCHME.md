---?image=assets/image/gitpitch-audience.jpg

# 12 principios que todo DevOps debe aplicar

---?image=assets/image/groucho.jpg&size=auto 80%

# Groucho Max

<p style="font-weigth:bolder; color: #e49436;">
 "Ladies and gentlemen, those are my principles, and if you don't like them I have some others" (Damas y caballeros, esos son mis principios, y si no les gusta, tengo otros).
</p>

---

### Principio 1: KISS (Keep It Simple Stupid)

- Todo diseño de software debe ser simple
- Todo desarrollo debe ser simple
- Evitar complejidades innecesarias
- Cuanto más sencillo mejor

---
### Principio 2: SOLID

Robert C.Martin para establecer los cinco principios básicos de P0O.

Objetivo fundamental: "Bajo acoplamiento y alta cohesión"

+++

- SRP (Single Responsability)→ las clases o módulos deben tener única responsabilidad
- Open/Close → abierto para extensión, cerrado para modificación
- Liskov → una clase derivada no debe modificar el comportamiento de la base base
- ISP → una clase que implementa una interfaz no debe depender de métodos que no utiliza. Interfaces sencillas y tener pocos métodos
- DIP (Dependency Inversion) → clases de alto nivel no deben depender clases de bajo nivel. Deben depender de abstracciones. Los detalles dependen de las abstracciones.

---
### Principio 3: SOC

- Separation of Concerns. (Separa los asuntos)
   - Capa de negocio
   - Capa servicio
   - Capa de datos

 Ejemplo: Patrón MVC

---
### Principio 4: YAGNI (You ain’t gonna need it)

“No implementes algo si no estamos seguro de necesitarlo”

---

### Principio 5: Regla del Boy Scout

“ Deja el campo más limpio que cuando lo encontraste “

Debemos refactorizar el código para dejarlo más limpio y simple que antes.

---

### Principio 6: Ley de Demeter

Una unidad sólo puede tener conocimiento limitado de otras unidades y solo de las que están relacionadas

Ejemplo:
	No acceder a métodos internos de un objeto que no conocemos

---

### Principio 7: Hollywood

“No nos llame, nosotros le llamaremos”

Una clase tiene referencias a objetos que necesita funcionar a través de una unidad externa, pero no conoce la implementación

Ejemplo: IoC == Spring

---

### Principio 8: GRASP

“object-oriented design General Responsibility Assignment Software Patterns”

+++

Principio básico de asignación de responsabilidades:
- Patrón creador
- Patrón controlador
- Alta cohesión y bajo acoplamiento
- Polimorfismo
- Fabricación pura
- Indirección
- Variaciones protegidas

---

### Principio 9: GoF

- Utiliza patrones de diseño: Gang of Four
- Abarcan el 100% de los problemas
- No te inventes diseños

---

### Principio 10: TDD

- Test driven development
- Piensa en las pruebas, luego programa
- Ventaja

---

### Principio 11: Occam

“En igualdad de condiciones la explicación más sencilla suele ser la correcta”

Significa que tú código debe tener o comportarse como:
 - Mantenibilidad
 - Extensibilidad
 - Reutilización

---

### Principio 12: Autocrítico

- Exige siempre lo mejor
- No aceptes cambios menores
- Proponte ser exigente con tú trabajo

